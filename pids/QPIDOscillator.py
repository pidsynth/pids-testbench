from PySide2.QtCore import Qt
from PySide2.QtWidgets import QGroupBox, QHBoxLayout, QPushButton, QVBoxLayout, QDoubleSpinBox

from commons.graphing.QGraphing import QGrapher
from commons.controls.QPIDControl import QPIDControl
from commons.controls.QPIDControlSignal import QPIDControlSignal

import numpy as np
import operator

from pids.pidOsc2 import SimplePIDOscillator


class QIndexedDoubleSpinBox(QDoubleSpinBox):
    def __init__(self, indexAt):
        super(QIndexedDoubleSpinBox, self).__init__()
        self.indexAt = indexAt


class QPIDOMapper(QGroupBox):

    def __init__(self, control_name="Untitled Breakpoint Mapper", totalBreakpoints=4):
        super(QPIDOMapper, self).__init__(control_name.strip())
        self.breakpointBoxes = list()
        self.breakpointValues = list()
        self.linearizeSetpointCurve = True

        self.addButton = QPushButton("Add")
        self.clearAllButton = QPushButton("Clear All")
        self.linearizeSetpointCurveButton = QPushButton("Set Linear Artist")
        self.breakpointsTable = QVBoxLayout()

        self.map_graph_signal = QPIDControlSignal()

        defaultMapperValues = [0, 1, -1]
        for i in range(totalBreakpoints - 1):
            self.addBreakpointRow(i / (totalBreakpoints - 1), defaultMapperValues[i % len(defaultMapperValues)])
        self.addBreakpointRow(1.0, float(self.breakpointBoxes[0][1].text()))

        self.updateBreakpoints()

        buttonLayout = QHBoxLayout()
        buttonLayout.addWidget(self.addButton)
        buttonLayout.addWidget(self.clearAllButton)

        completeMapTableLayout = QVBoxLayout()
        completeMapTableLayout.addLayout(buttonLayout)
        completeMapTableLayout.addWidget(self.linearizeSetpointCurveButton)
        completeMapTableLayout.addLayout(self.breakpointsTable)

        self.setLayout(completeMapTableLayout)

        self.addButton.pressed.connect(
            lambda: self.addBreakpointRow(np.random.uniform(-0.99, 0.99), np.random.uniform(-0.99, 0.99),
                                          graphDraw=True))
        self.clearAllButton.pressed.connect(lambda: self.clearAllBreakpoints(layout=self.breakpointsTable, reset=True))
        self.linearizeSetpointCurveButton.pressed.connect(self.toggleLinearizeSetpointCurveButton)

    def addBreakpointRow(self, xValue, yValue, graphDraw=False):
        xLabel = QDoubleSpinBox()
        xLabel.setMinimum(-1.0)
        xLabel.setMaximum(1.0)
        xLabel.setSingleStep(0.01)

        yLabel = QDoubleSpinBox()
        yLabel.setMinimum(-1.0)
        yLabel.setMaximum(1.0)
        yLabel.setSingleStep(0.01)

        xLabel.setValue(xValue)
        yLabel.setValue(yValue)

        self.breakpointBoxes.append([xLabel, yLabel])

        breakpointsTableRow = QHBoxLayout()
        breakpointsTableRow.addWidget(xLabel)
        breakpointsTableRow.addWidget(yLabel)
        self.breakpointsTable.addLayout(breakpointsTableRow)

        if graphDraw:
            self.map_graph_signal.askToCreateGraph()

        xLabel.textChanged.connect(self.updateBreakpoints)
        yLabel.textChanged.connect(self.updateBreakpoints)

        print("[PIDO Mapper] => Added new breakpoint: [{}, {}]".format(xValue, yValue))

    def clearAllBreakpoints(self, layout, reset=False):
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.clearAllBreakpoints(item.layout())

        if reset:
            self.breakpointBoxes.clear()
            print("[PIDO Mapper] => Cleared all breakpoints")
            self.addBreakpointRow(0.0, np.random.uniform(0.0, 1.0))
            self.addBreakpointRow(1.0, np.random.uniform(0.0, 1.0), graphDraw=True)

    def updateBreakpoints(self):
        self.breakpointValues.clear()
        for row in self.breakpointBoxes:
            self.breakpointValues.append([float(row[0].text()), float(row[1].text())])

        self.breakpointValues.sort(key=operator.itemgetter(0))
        print("Breakpoints = {}".format(self.breakpointValues))
        self.map_graph_signal.askToCreateGraph()

    def sortBreakpoints(self):
        for i in range(len(self.breakpointBoxes)):
            for j in range(i + 1, len(self.breakpointBoxes)):
                if self.breakpointBoxes[i][0] > self.breakpointBoxes[j][0]:
                    temp = self.breakpointBoxes[i]
                    self.breakpointBoxes[i] = self.breakpointBoxes[j]
                    self.breakpointBoxes[j] = temp

    def toggleLinearizeSetpointCurveButton(self):
        if self.linearizeSetpointCurve:
            self.linearizeSetpointCurveButton.setText("Set Step Artist")
            self.linearizeSetpointCurve = False
        else:
            self.linearizeSetpointCurveButton.setText("Set Linear Artist")
            self.linearizeSetpointCurve = True
        print("[PIDO Mapper] => Linearization of artist set to {}".format(self.linearizeSetpointCurve))
        self.map_graph_signal.askToCreateGraph()


# class QPIDOControl(QPIDControl):
#     def __init__(self, parameterName, spec):
#         super(QPIDOControl, self).__init__(parameterName, spec)
#         self.controlSource = QComboBox()
#         self.osc.addWidget(self.controlSource, Qt.AlignCenter)


class QPIDOscillator(QGroupBox):

    def __init__(self, oscillatorName='Untitled Oscillator', breakpoints=4, parameterNames=['Kp', 'Ki', 'Kd', 'Ratio'],
                 specs=[[-10, 5], [-10, 5], [-10, 5], [-10, 0]]):
        self.oscName = oscillatorName.strip()
        super(QPIDOscillator, self).__init__(self.oscName)

        self.samplingRate = 44100
        self.currentFreq = 440

        self.oscRows = QHBoxLayout()
        self.graphing = QGrapher(samplingRate=self.samplingRate)
        self.controls = dict()
        self.breakpoints = breakpoints
        self.parameterNames = parameterNames

        self.setpointCurveDisplayToggleButton = QPushButton("Show Artist")
        self.setpointCurveDisplay = False
        self.setpointCurveDisplayToggleButton.pressed.connect(self.updateSetpointCurveDisplayState)

        self.plotStyleToggleButton = QPushButton("Change to Freq. Plot")
        self.isFreqPlot = False
        self.plotStyleToggleButton.pressed.connect(self.updateGraphingStyle)

        self.oscOutputSamples = list()
        self.oscSetpointSamples = list()
        self.triggerToDrawGraphsSignal = QPIDControlSignal()

        for i in range(len(self.parameterNames)):
            self.controls[self.parameterNames[i]] = QPIDControl(self.parameterNames[i], spec=specs[i])
            self.oscRows.addWidget(self.controls[self.parameterNames[i]])
            self.controls[self.parameterNames[i]].drawGraphSignal.interfaced.connect(self.updateOscGraphing)

        self.breakpointMapTable = QPIDOMapper(control_name="PIDO Breakpoints")
        self.breakpointMapTable.map_graph_signal.interfaced.connect(self.updateOscGraphing)

        self.completeOscLayout = QVBoxLayout()
        self.controlLayout = QHBoxLayout()
        self.completeOscLayout.addLayout(self.oscRows)
        self.completeOscLayout.addWidget(self.graphing)
        self.bottomButtonLayout = QHBoxLayout()
        self.bottomButtonLayout.addWidget(self.setpointCurveDisplayToggleButton, Qt.AnchorRight)
        self.bottomButtonLayout.addWidget(self.plotStyleToggleButton, Qt.AnchorRight)
        self.completeOscLayout.addLayout(self.bottomButtonLayout)
        self.controlLayout.addLayout(self.completeOscLayout)
        self.controlLayout.addWidget(self.breakpointMapTable)
        self.setLayout(self.controlLayout)

    def setCurrentFrequency(self, frequency):
        self.currentFreq = frequency

    def fillInControlSources(self, controlSources):
        otherControlSources = [source.strip() for source in controlSources if source.strip() != self.oscName]
        for i in range(len(self.parameterNames)):
            self.controls[self.parameterNames[i]].fillInControlSources(otherControlSources)

    def updateOscGraphing(self):
        self.oscOutputSamples, self.oscSetpointSamples = SimplePIDOscillator(sampling_rate=self.samplingRate,
                                                                             breakpoints=self.breakpointMapTable.breakpointValues,
                                                                             frequency=self.currentFreq,
                                                                             Kp=self.controls['Kp'].controlValue,
                                                                             Ki=self.controls['Ki'].controlValue,
                                                                             Kd=self.controls['Kd'].controlValue,
                                                                             discontinousArtist=self.breakpointMapTable.linearizeSetpointCurve).drawOscillatorSamples()
        if self.isFreqPlot:
            if self.setpointCurveDisplay:
                self.graphing.set_freq_graph(src='SP', y=self.oscSetpointSamples)
            else:
                self.graphing.set_freq_graph(src='SP', y=[0])
            self.graphing.set_freq_graph(src='OP', y=self.oscOutputSamples)
        else:
            if self.setpointCurveDisplay:
                self.graphing.set_graph(src='SP', y=self.oscSetpointSamples)
            else:
                self.graphing.set_graph(src='SP', y=[0])
            self.graphing.set_graph(src='OP', y=self.oscOutputSamples)
        self.triggerToDrawGraphsSignal.askToCreateGraph()

    def givePlayerSamples(self, sampling_rate=44100, frequency=440, duration_s=3):
        return SimplePIDOscillator(sampling_rate=sampling_rate,
                                   breakpoints=self.breakpointMapTable.breakpointValues,
                                   frequency=frequency,
                                   Kp=self.controls['Kp'].controlValue,
                                   Ki=self.controls['Ki'].controlValue,
                                   Kd=self.controls['Kd'].controlValue,
                                   discontinousArtist=self.breakpointMapTable.linearizeSetpointCurve,
                                   oversample_by=4).drawOscillatorSamples(
            duration_s=duration_s, returnSetpointCurve=False)

    def updateSetpointCurveDisplayState(self):
        if self.setpointCurveDisplay:
            self.setpointCurveDisplay = False
            self.updateOscGraphing()
            self.setpointCurveDisplayToggleButton.setText("Show Artist")
        else:
            self.setpointCurveDisplay = True
            self.updateOscGraphing()
            self.setpointCurveDisplayToggleButton.setText("Hide Artist")
        print("[{}->CURVE VISIBILITY CHANGE] => Setpoint visibility set to {}".format(self.oscName,
                                                                                      self.setpointCurveDisplay))

    def updateGraphingStyle(self):
        if self.isFreqPlot:
            self.isFreqPlot = False
            self.updateOscGraphing()
            self.plotStyleToggleButton.setText("Change to Freq. Plot")
            print("[{}->PLOT STYLE CHANGE] => Plot style set to Time Plot".format(self.oscName))
        else:
            self.isFreqPlot = True
            self.updateOscGraphing()
            self.plotStyleToggleButton.setText("Change to Time Plot")
            print("[{}->PLOT STYLE CHANGE] => Plot style set to Frequency Plot".format(self.oscName))

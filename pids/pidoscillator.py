import numpy as np
from collections import deque


class PIDOscillator:
    def __init__(self, breakpoints, sampling_rate=44100, frequency=1, Kp=0.001, Ki=0.0004, Kd=0.001, wet=1,
                 linearize_setpoint_curve=False):
        self.samplingRate = sampling_rate
        self.frequency = frequency

        self.inputPIDO = input
        self.inputPIDOMaps = [False, False, False, False, False]

        self.breakpointValues = breakpoints

        self.parameters = [Kp, Ki, Kd, wet]

        self.dt = 0.1 * self.samplingRate

        self.setPoint = 0
        self.measuredValue = 0
        self.output = 0
        self.integral = 0
        self.previousError = 0
        self.errors = deque(maxlen=3)
        for i in range(self.errors.maxlen):
            self.errors.append(0)

        self.setpointCurve = list()
        self.LFO = list()
        self.linearizeSetpointCurve = linearize_setpoint_curve

    def initVariables(self):
        self.setPoint = 0
        self.measuredValue = 0
        self.output = 0
        self.integral = 0
        self.previousError = 0
        self.errors = deque(maxlen=3)
        for i in range(self.errors.maxlen):
            self.errors.append(0)

        self.setpointCurve.clear()
        self.LFO.clear()

    def drawLFO(self, duration_s=1, returnSetpointCurve=True):
        self.initVariables()

        if self.linearizeSetpointCurve:
            for i in range(1, len(self.breakpointValues)):
                self.setpointCurve.extend(np.linspace(self.breakpointValues[i - 1][1], self.breakpointValues[i][1],
                                                      round((self.breakpointValues[i][0] - self.breakpointValues[i - 1][
                                                          0]) * self.samplingRate / self.frequency)))
        else:
            for i in range(1, len(self.breakpointValues)):
                self.setpointCurve.extend([self.breakpointValues[i - 1][1]] *
                                          round((self.breakpointValues[i][0] - self.breakpointValues[i - 1][
                                              0]) * self.samplingRate / self.frequency))

        if self.linearizeSetpointCurve:
            self.measuredValue = self.setpointCurve[0]
        else:
            self.measuredValue = 0

        for i in range(self.frequency * duration_s):
            for setpoint in self.setpointCurve:
                if pidMode == 1:
                    self.pid1(setpoint)
                else:
                    self.pid2(setpoint)

                self.measuredValue += self.output
                self.measuredValue = max(-1, min(1, self.measuredValue))

                self.LFO.append(self.measuredValue)

        if returnSetpointCurve:
            extSetpointCurve = list()
            for i in range(self.frequency * duration_s):
                extSetpointCurve.extend(self.setpointCurve)
            return self.LFO, extSetpointCurve
        return self.LFO

    def pid1(self, setpoint):
        error = setpoint - self.measuredValue
        self.integral += error
        # self.output = self.parameters[0] * error + self.parameters[1] * self.integral + self.parameters[2] * (
        #         error - self.previousError) / (self.dt / self.sr)
        self.output = self.parameters[0] * error + self.parameters[1] * self.integral + self.parameters[2] * \
                      (error - self.previousError)
        self.previousError = error

    def pid2(self, setpoint):
        T = 0.1
        K1 = self.parameters[0] + T * self.parameters[1] / 2 + self.parameters[2] / T
        K2 = -self.parameters[0] + T * self.parameters[1] / 2 - 2 * self.parameters[2] / T
        K3 = self.parameters[2] / T

        self.errors.append(setpoint - self.measuredValue)
        self.output = self.output + K1 * self.errors[2] + K2 * self.errors[1] + K3 * self.errors[0]


def test():
    from matplotlib import pyplot as plt
    freq = 4400
    breakpoints = [[0, 0.5], [0.2, 0.3], [0.6, 0.7], [1, 0]]

    pidlfo = PIDOscillator(breakpoints=breakpoints, frequency=freq, Kp=0.02, Ki=0.01, Kd=0.1, wet=1)
    lfo2, sv = pidlfo.drawLFO(pidMode=2)

    pidlfo = PIDOscillator(breakpoints=breakpoints, frequency=freq, Kp=0.02, Ki=0.01, Kd=0.1, wet=1)
    lfo1, sv = pidlfo.drawLFO(pidMode=1)

    plt.plot(sv[:2000], label="SV")
    # plt.plot(lfo2[:2000], label="LFO2")
    plt.plot(lfo1[:2000], label="LFO1")
    plt.title("{}Hz LFO".format(freq))
    plt.legend()

    print(lfo2)
    from commons.player import wavePlayer
    wavePlayer.play(lfo2)
    plt.show()


if __name__ == '__main__':
    test()

from PySide2.QtWidgets import QGroupBox, QVBoxLayout

from commons.graphing.QGraphing import QGrapher

from commons.player.oscillatormixer import oscillatorMixer


class QPIDOscillatorMixer(QGroupBox):

    def __init__(self, oscillatorSources, mixer_name="Untitled Oscillator Mixer"):
        self.mixerName = mixer_name.strip()
        super(QPIDOscillatorMixer, self).__init__(self.mixerName)
        self.oscillatorSources = oscillatorSources
        for oscillatorSource in self.oscillatorSources:
            oscillatorSource.triggerToDrawGraphsSignal.interfaced.connect(self.updateMixerGraph)
        self.mixedOutput = None

        self.graphing = QGrapher()

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.graphing)
        self.setLayout(self.layout)

    def mixOscillatorOutputs(self):
        oscillatorSamplesAndRatios = list()
        for source in self.oscillatorSources:
            oscillatorSamplesAndRatios.append([source.oscOutputSamples, source.controls['Ratio'].controlValue])
        self.mixedOutput = oscillatorMixer(oscillatorSamplesAndRatios=oscillatorSamplesAndRatios)

    def updateMixerGraph(self):
        self.mixOscillatorOutputs()
        self.graphing.set_graph(src='OP', y=self.mixedOutput)

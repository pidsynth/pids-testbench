import numpy as np
from scipy.signal import butter, lfilter

from commons.algorithms.oversample import Antialiaser


class SimplePIDOscillator:
    def __init__(self, breakpoints, sampling_rate=44100, frequency=1, Kp=0.001, Ki=0.0004, Kd=0.001, wet=1,
                 discontinousArtist=False, integralWindingLimit=5000, oversample_by=1, sineArtist=False):
        self.samplingRate = sampling_rate
        self.oversampling = oversample_by
        self.frequency = frequency
        # self.quantizer = round(self.samplingRate / self.frequency)
        self.quantizer = self.oversampling * self.samplingRate / self.frequency

        self.breakpointValues = breakpoints

        self.parameters = [Kp, Ki, Kd, wet]

        self.setPoint = 0
        self.measuredValue = 0
        self.integral = 0
        self.previousError = 0
        self.phasor = 0

        self.discontinousArtist = discontinousArtist
        self.wavefolding = False
        self.antialiaser = Antialiaser(self, oversample_by=self.oversampling)

        self.integralWindingLimit = integralWindingLimit
        self.sineArtist = sineArtist

    def initVariables(self):
        self.setPoint = 0
        self.measuredValue = 0
        self.integral = 0
        self.previousError = 0
        self.phasor = 0
        # self.recentSamples = [0 for i in range(int(self.quantizer))]
        # self.recentSamplesScroll = 0
        # self.mean = 0

    def setFrequency(self, frequency):
        self.frequency = frequency
        self.quantizer = self.samplingRate / self.frequency

    def polyBLEP(self, t):
        # dt = 1 / self.quantizer
        # if t < dt:
        #     t /= dt
        #     return t + t - t * t - 1.0
        # elif t > 1.0 - dt:
        #     t = (t - 1.0) / dt
        #     return t * t + t + t + 1.0
        return 0

    def get_next_setpoint(self):
        if self.sineArtist:
            return np.sin(2 * np.pi * self.phasor) - self.polyBLEP(self.phasor)
        elif self.discontinousArtist:
            for i in range(1, len(self.breakpointValues)):
                if self.phasor < self.breakpointValues[i][0]:
                    return self.breakpointValues[i][1] + self.polyBLEP((self.phasor + 0.5) % 1.0) - self.polyBLEP(
                        self.phasor)
        else:
            for i in range(1, len(self.breakpointValues)):
                if self.phasor < self.breakpointValues[i][0]:
                    return (self.breakpointValues[i][1] - self.breakpointValues[i - 1][1]) \
                           * (self.phasor - self.breakpointValues[i - 1][0]) \
                           / (self.breakpointValues[i][0] - self.breakpointValues[i - 1][0]) \
                           + self.breakpointValues[i - 1][1] - self.polyBLEP(self.phasor)

    def get_next_sample(self):

        self.setPoint = self.get_next_setpoint()

        error = self.setPoint - self.measuredValue
        self.integral += error

        output = self.parameters[0] * error + self.parameters[1] * self.integral + self.parameters[2] * (
                error - self.previousError)

        self.previousError = error
        self.phasor += 1 / self.quantizer
        if self.phasor > 1:
            self.phasor -= 1

        self.measuredValue = max(-self.integralWindingLimit, min(self.integralWindingLimit,
                                                                 self.measuredValue + output))

        if self.wavefolding:
            # TODO Following logic will suit only for -2<=measuredValue<=2. Find a better solution for this!
            if self.measuredValue > 1:
                thisSample = 2 - self.measuredValue
            elif self.measuredValue < -1:
                thisSample = -2 - self.measuredValue
            else:
                thisSample = self.measuredValue
        else:
            thisSample = min(1, max(-1, self.measuredValue))
        return self.setPoint, thisSample

    def drawOscillatorSamples(self, duration_s=1, returnSetpointCurve=True):
        self.initVariables()

        setPointSamples = list()
        oscSamples = list()
        for i in range(int(duration_s * self.samplingRate)):
            setpoint, oscSample = self.antialiaser.pidSynthAntialiase()
            oscSamples.append(oscSample)
            setPointSamples.append(setpoint)

        # oscSamples = self.butter_lowpass_filter(oscSamples, 20000, self.samplingRate)

        if returnSetpointCurve:
            return oscSamples, setPointSamples
        return oscSamples

    def butter_lowpass(self, cutoff, fs, order=5):
        nyq = 0.5 * fs
        normal_cutoff = cutoff / nyq
        b, a = butter(order, normal_cutoff, btype='low', analog=False)
        return b, a

    def butter_lowpass_filter(self, data, cutoff, fs, order=5):
        b, a = self.butter_lowpass(cutoff, fs, order=order)
        y = lfilter(b, a, data)
        return y


def test():
    from matplotlib import pyplot as plt
    SR = 44100
    freq = 440
    breakpoints = [[0, 0], [0.33, 1], [0.67, -1], [1, 0]]

    pidlfo = SimplePIDOscillator(breakpoints=breakpoints, frequency=freq, Kp=10 ** -0.63 - 0.00000001, Ki=0,
                                 Kd=0, wet=1, discontinousArtist=False, sampling_rate=SR)
    lfo2, sv = pidlfo.drawOscillatorSamples(duration_s=3)
    # lfo2 = butter_lowpass_filter(lfo2, 10000, SR, 5)
    # print(lfo2)

    # pidlfo = SimplePIDOscillator(breakpoints=breakpoints, frequency=freq, Kp=0.02, Ki=0.01, Kd=0.1, wet=1)
    # lfo1, sv = pidlfo.drawLFO(duration_s=3)

    from commons.player import wavePlayer
    wavePlayer.play(lfo2)

    plt.plot(sv[:200], label="SV")
    # plt.plot(lfo2[:2000], label="LFO2")
    plt.plot(lfo2[:200], label="LFO1")
    plt.title("{}Hz Wave".format(freq))
    plt.legend()

    # yf = fft(lfo2)
    # xf = fftfreq(len(lfo2), 1 / SR)
    # plt.plot(xf, np.abs(yf))

    plt.show()


if __name__ == '__main__':
    test()

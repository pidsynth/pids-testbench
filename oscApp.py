import sys
from PySide2.QtWidgets import QApplication, QWidget, QHBoxLayout, QGridLayout
from pids.QPIDOscillator import QPIDOscillator
from pids.QPIDOscillatorMixer import QPIDOscillatorMixer
from commons.player.QSoundPlayer import QSoundPlayer
from commons.theme.dark import setDarkPalette

app = QApplication()
app.setApplicationName("PIDS")
setDarkPalette(app)

oscNames = ["PIDS Osc 1", "PIDS Osc 2"]
oscillators = list()
for i, osc in enumerate(oscNames):
    oscillators.append(QPIDOscillator(oscillatorName=osc))
    oscillators[i].fillInControlSources(oscNames)

mixer = QPIDOscillatorMixer(oscillatorSources=oscillators, mixer_name="PIDS Osc Mixer")

player = QSoundPlayer(soundDrivers=oscillators)

mainLayout = QGridLayout()
oscLayout = QHBoxLayout()
for oscillator in oscillators:
    oscLayout.addWidget(oscillator)

mainLayout.addLayout(oscLayout, 1, 0, 1, 1)
mainLayout.addWidget(mixer, 2, 0, 1, 1)
mainLayout.addWidget(player, 3, 0, 1, 1)

mainApp = QWidget()
mainApp.setLayout(mainLayout)
mainApp.show()

sys.exit(app.exec_())

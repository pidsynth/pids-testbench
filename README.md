# PID Synthesis Testbench GUI

The PID Synthesis (PIDS) Testbench is a QT 5.x based graphical application for understanding, testing and evaluating the [PIDS](https://arxiv.org/abs/2109.10455) specification.

<p align="center">
  <img src="docs/screen.png" />
</p>

The testbench is developed parallel to the development of the framework and is extensively used in protoyping it.

Some of its salient features include:

- Interactive controls for each of the PIDS input parameters:
  - PID gains & mixing ratio values can either be provided directly in numeric form (infinite range) or be modulated via the control knob. Knob range for:
    - Kp, Ki and Kd: [10^-10, 10^5]
    - Ratio: [10^-10, 1]
  - Breakpoints can be set only via numeric form. Range for:
    - X-Values: [0, 1]
    - Y-Values: [-1, 1]
- Real-time plotting of the PIDS `output` for the provided parameter inputs. Effects of automating any control parameter can be conviniently visualized by left-clicking and moving the cursor across the respective knob / numeric increment button. *The plot renders a maximum 44,100 synthesized `output` samples.*
- The plot can also optionally toggle the visibility of the `artist` (aka the setpoint curve) against which the PIDS `output` is generated.
- The frequency spectrum of the synthesized `output` & `artist` can be obtained in real-time by toggling the corresponding button on the UI.
- Provides facility to define infinite number of new intermediate breakpoints. The default is 4 and the minimum is 2.
- Provides facility to toggle between step (default) & linear `artist` skeletons via the UI and selecting the sine `artist` via in-code modification.
- Provides an integrated sound player that can be used to play out the rendered master `output` at the desired frequency & sampling rate and for the desired duration. *Note: Changing the frequency parameter will also update all the graphs to render `outputs` at the set value.*
- The per-PIDS parameter `Ratio` is useful when multiple PIDS oscillator instances are used. In this case, it is used to set the overall ratio by which each constituent PIDS `output` is mixed to form the master `output`. *Note: The testbench can support any number of PIDS oscillators operating simultaneously. However, they require code changes to be made in `oscApp.py`. Support will be provided to specify them via arguments in later releases.*
- Supports setting the oversampling magnitude for anti-aliasing. *Available options are 1x, 2x, 4x & 8x*. However, this can be done only via in-code modifications currently.
- Detailed logging is provided through the console to track internal work flow.
  
## Installation

### Prerequisite

- Python 3.8+

### Linux

1. Clone this repo to local, switch to local repo directory

```sh
git clone --recursive https://gitlab.com/pidosc/pids-testbench.git
cd pids-testbench
```

2. Create a virtual environment inside repo

```sh
python -m venv venv 
```

3. Activate venv

```sh
# for bash-compatible shells 
source venv/bin/activate

# for fish, csh, etc use appropriate activation scripts inside venv/bin/
```

4. Install required python dependencies

```sh
#upgrade pip if required
pip install --upgrade pip

#install all requirements
pip install -r requirements.txt
```

## Usage

### Linux

1. Add all sources to PYTHONPATH

```sh
export PYTHONPATH="${PYTHONPATH}:<path_to_local_repo>:<path_to_local_repo>/commons"
```

2. Run `oscApp.py`

```sh
python oscApp.py 
```

### Interacting with the GUI

Feel free to vary any of the available control knobs and numeric inputs as desired. Users may also optionally toggle the setpoint curve visibility to compare it with the generated envelope. At any point, the `output` displayed on the master plot can be played out using the sound player. Users have the option to change the frequency, amplitude, sampling rate and duration of the synthesized `output`.

Refer the video:

[![PIDS Demo](docs/video_placeholder.jpg)](https://youtu.be/KAqW8yUVfBk "PIDS Demo")

## Contributing

0. [Report Bugs or Request Features](https://gitlab.com/pidosc/pids-testbench/-/issues)
1. Fork it (<https://gitlab.com/pidosc/pids-testbench>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Merge/Pull Request

## License

Distributed under the GNU General Public License v3.0 License. See `LICENSE` for more information.

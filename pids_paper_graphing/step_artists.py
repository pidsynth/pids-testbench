import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fftfreq
from scipy.fft import fft
from scipy.signal.windows import blackman

from pids.pidOsc2 import SimplePIDOscillator


def convert2Freq(y):
    N = len(y)
    w = blackman(N)

    ywf = fft(y * w)
    xf = fftfreq(N, 1.0 / 44100)[:N // 2]

    return xf[1:N // 2], 2.0 / N * np.abs(ywf[1:N // 2])

def breakpoint_string(pidlfo):
    str = r'\\Breakpoints:'
    for bp in pidlfo.breakpointValues:
        str += r'\\({},{})'.format(bp[0], bp[1])
    return r'${}$'.format(str)


font_size = 6
plt.style.use(['science', 'ieee'])
plt.rcParams.update({"figure.dpi": "500",
                     "font.size": font_size,
                     "axes.prop_cycle": "(cycler('color', ['black', 'dimgrey', 'grey', 'darkgrey','lightgrey']))"})

Kp = 0.3
Ki = 0.8
Kd = 0

x = np.arange(0, 10, 1 / 44.1)
pidlfo = SimplePIDOscillator(breakpoints=[[0, 0], [0.33, 1], [0.67, -1], [1, 0]], frequency=440,
                             Kp=Kp, Ki=Ki, Kd=Kd,
                             wet=1, discontinousArtist=True, sampling_rate=44100, integralWindingLimit=1)
a, y1 = pidlfo.drawOscillatorSamples(duration_s=0.01, returnSetpointCurve=True)
ax0 = plt.subplot(111)
line0, = ax0.plot(x, y1, color='tab:red', linewidth=0.5)
ax0.set_ylabel('$Amplitude\ (in\ V)$', fontsize=font_size * 1.2)
ax0.set_xlabel('$Duration\ (in\ ms)$', fontsize=font_size * 1.2)
ax0.text(max(x), max(y1), breakpoint_string(pidlfo), linespacing=0.01,
         fontsize=font_size, bbox=dict(facecolor='white', edgecolor='k', linewidth=0.5, alpha=0.7),
         horizontalalignment='right', verticalalignment='top')
plt.show()

plt.cla()

pidlfo = SimplePIDOscillator(breakpoints=[[0, 0.33], [0.33, -0.8], [0.67, -0.43], [1, 0.33]], frequency=440,
                             Kp=Kp, Ki=Ki, Kd=Kd,
                             wet=1, discontinousArtist=True, sampling_rate=44100)
a, y2 = pidlfo.drawOscillatorSamples(duration_s=0.01, returnSetpointCurve=True)
ax1 = plt.subplot(111)
ax1.plot(x, y2, color='tab:blue', linewidth=0.5)
ax1.set_ylabel('$Amplitude\ (in\ V)$', fontsize=font_size * 1.2)
ax1.set_xlabel('$Duration\ (in\ ms)$', fontsize=font_size * 1.2)
ax1.text(max(x), max(y2), breakpoint_string(pidlfo), linespacing=0.01,
         fontsize=font_size, bbox=dict(facecolor='white', edgecolor='k', linewidth=0.5, alpha=0.7),
         horizontalalignment='right', verticalalignment='top')
plt.show()

plt.cla()

pidlfo = SimplePIDOscillator(breakpoints=[[0, 0], [0.2, 0.5], [0.85, -0.75], [1, 0]], frequency=440,
                             Kp=Kp, Ki=Ki, Kd=Kd,
                             wet=1, discontinousArtist=True, sampling_rate=44100)
a, y3 = pidlfo.drawOscillatorSamples(duration_s=0.01, returnSetpointCurve=True)
ax1 = plt.subplot(111)
ax1.plot(x, y3, color='tab:green', linewidth=0.5)
ax1.set_ylabel('$Amplitude\ (in\ V)$', fontsize=font_size * 1.2)
ax1.set_xlabel('$Duration\ (in\ ms)$', fontsize=font_size * 1.2)
ax1.text(max(x), max(y3), breakpoint_string(pidlfo), linespacing=0.01,
         fontsize=font_size, bbox=dict(facecolor='white', edgecolor='k', linewidth=0.5, alpha=0.7),
         horizontalalignment='right', verticalalignment='top')
plt.show()

plt.cla()

pidlfo = SimplePIDOscillator(breakpoints=[[0, 1], [0.25, -1], [0.75, -1], [1, 1]], frequency=440,
                             Kp=Kp, Ki=Ki, Kd=Kd,
                             wet=1, discontinousArtist=True, sampling_rate=44100)
a, y4 = pidlfo.drawOscillatorSamples(duration_s=0.01, returnSetpointCurve=True)
ax1 = plt.subplot(111)
ax1.plot(x, y4, color='tab:orange', linewidth=0.5)
ax1.set_ylabel('$Amplitude\ (in\ V)$', fontsize=font_size * 1.2)
ax1.set_xlabel('$Duration\ (in\ m s)$', fontsize=font_size * 1.2)
ax1.text(max(x), max(y4), breakpoint_string(pidlfo), linespacing=0.01,
         fontsize=font_size, bbox=dict(facecolor='white', edgecolor='k', linewidth=0.5, alpha=0.7),
         horizontalalignment='right', verticalalignment='top')
plt.show()

# ax1.set_yticks([-1, 1])

# legend = ax2.legend((line0, line1, line2), ('PIDS1', 'PIDS2', 'Combined\nOutput'), handlelength=1.5, handletextpad=0.25,
#                     fancybox=False, loc='lower right', frameon=True,
#                     prop=dict(size=font_size))
# frame = legend.get_frame()
# frame.set_facecolor('white')
# frame.set_edgecolor('k')
# frame.set_linewidth(0.5)
# frame.set_alpha(0.7)

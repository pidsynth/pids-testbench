import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from numpy.fft import fftfreq
from scipy.fft import fft
from scipy.signal.windows import blackman

from pids.pidOsc2 import SimplePIDOscillator


def log_tick_formatter(val, pos=None):
    return "{:.2e}".format(10 ** val)


def convert2Freq(y):
    N = len(y)
    w = blackman(N)

    ywf = fft(y * w)
    xf = fftfreq(N, 1.0 / 44100)[:N // 2]

    return xf[1:N // 2], 2.0 / N * np.abs(ywf[1:N // 2])


cmap = matplotlib.cm.get_cmap('Dark2')

fig = plt.figure(figsize=(12, 10))

# syntax for 3-D projection
ax = plt.axes(projection='3d')

freq_waves = list()
ps = list()

# for PID
# p_range = [np.log10(i) for i in np.arange(2, 10, 0.5)]
p_range = [np.log10(i) for i in np.arange(1.05, 2, 0.1)]
# p_range.extend([np.log10(i) for i in np.arange(1.05, 2, 0.1)])
# p_range = [10 ** i for i in np.arange(-3, 0, 0.1)]
# p_range = np.arange(0.01, 1, 0.01)

# for freq (Kp = 0.6, Ki = 0.3)
# p_range = [440 * (2 ** (i / 12)) for i in np.arange(-12, 24, 3)]
freq_plot = True
colour_shift = 0
for p in p_range:
    pidlfo = SimplePIDOscillator(breakpoints=[[0, 0], [0.33, 1], [0.67, -1], [1, 0]], frequency=440,
                                 Kp=p, Ki=0, Kd=0,
                                 wet=1, discontinousArtist=True, sampling_rate=44100, oversample_by=4)
    pidlfo.setFrequency(440)
    y = pidlfo.drawOscillatorSamples(duration_s=0.01, returnSetpointCurve=False)
    ps.append(p)
    freq_waves.append(y)
    if freq_plot:
        X, Y = convert2Freq(y)
        ax.plot3D(X, Y, p, color=cmap(colour_shift), zdir='x')
    else:
        ax.plot3D(np.arange(0, len(y)) / 44.1, y, p, color=cmap(colour_shift), zdir='x')
    colour_shift = (colour_shift + 0.1) % 1
    # ax.xaxis.set_major_formatter(mticker.FuncFormatter(log_tick_formatter))

ps = np.asarray(ps)
freq_waves = np.asarray(freq_waves)

x = np.arange(0, len(freq_waves[0])) / 44.1
X, Y = np.meshgrid(x, ps)

Z = freq_waves

# ax.plot_surface(X, Y, Z, cmap="cividis", lw=0, rstride=1, cstride=1)
# ax.set_xlabel('$Artist\ Fundamental\ Frequency\ (in\ Hz)$', fontsize=20, labelpad=10)
ax.set_xlabel('$K_{d}$', fontsize=20, labelpad=10)
if freq_plot:
    ax.set_ylabel('$Frequency\ (in\ Hz)$', fontsize=20, labelpad=10)
    ax.set_zlabel('$Magnitude\ (in\ V)$', fontsize=20, labelpad=10)
else:
    ax.set_ylabel('$Duration\ (in\ ms)$', fontsize=20, labelpad=10)
    ax.set_zlabel('$Amplitude\ (in\ V)$', fontsize=20, labelpad=10)

plt.tight_layout()
plt.show()

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import gridspec

from pids.pidOsc2 import SimplePIDOscillator

font_size = 6
plt.style.use(['science', 'ieee'])
plt.rcParams.update({"figure.dpi": "500",
                     "font.size": font_size,
                     "axes.prop_cycle": "(cycler('color', ['black', 'dimgrey', 'grey', 'darkgrey','lightgrey']))"})

fig = plt.figure()
gs = gridspec.GridSpec(2, 2)

x = np.arange(0, 10, 1 / 44.1)
pidlfo = SimplePIDOscillator(breakpoints=[[0, 0], [0.33, 1], [0.67, -1], [1, 0]], frequency=440,
                             Kp=0.6, Ki=0.3, Kd=0,
                             wet=1, discontinousArtist=True, sampling_rate=44100)
y1 = pidlfo.drawOscillatorSamples(duration_s=0.01, returnSetpointCurve=False)
ax0 = plt.subplot(gs[0, 0])
# ax0.set_yscale("log")
line0, = ax0.plot(x, y1, color='tab:red', linewidth=0.5)
ax0.set_ylabel('$Amplitude\ (in\ V)$', fontsize=font_size * 1.2)
# ax0.set_xlabel("$Duration\ (in\ ms)$", fontsize=font_size * 1.2)
ax0.set_yticks([-1, 1])

pidlfo = SimplePIDOscillator(breakpoints=[[0, 0.33], [0.25, -0.75], [0.75, 0.6], [1, 0.33]], frequency=440,
                             Kp=0.3, Ki=0.8, Kd=0,
                             wet=1, discontinousArtist=False, sampling_rate=44100)
y2 = pidlfo.drawOscillatorSamples(duration_s=0.01, returnSetpointCurve=False)
ax1 = plt.subplot(gs[1, 0], sharex=ax0)
line1, = ax1.plot(x, y2, color='tab:blue', linewidth=0.5)
# plt.setp(ax1.get_xticklabels(), visible=False)
yticks = ax1.yaxis.get_major_ticks()
yticks[-1].label1.set_visible(False)
ax1.set_ylabel('$Amplitude\ (in\ V)$', fontsize=font_size * 1.2)
ax1.set_xlabel("$Duration\ (in\ ms)$", fontsize=font_size * 1.2)
ax1.set_yticks([-1, 1])

y3 = [y1[i] * 0.6 + y2[i] * 0.4 for i in range(len(y1))]
ax2 = plt.subplot(gs[:2, 1])
line2, = ax2.plot(x, y3, color='tab:purple', linewidth=0.5)
ax2.set_ylabel('$Amplitude\ (in\ V)$', fontsize=font_size * 1.2)
ax2.set_xlabel("$Duration\ (in\ ms)$", fontsize=font_size * 1.2)
ax2.yaxis.set_label_position("right")

legend = ax2.legend((line0, line1, line2), ('PIDS1', 'PIDS2', 'Combined\nOutput'), handlelength=1.5, handletextpad=0.25,
                    fancybox=False, loc='lower right', frameon=True,
                    prop=dict(size=font_size))
frame = legend.get_frame()
frame.set_facecolor('white')
frame.set_edgecolor('k')
frame.set_linewidth(0.5)
frame.set_alpha(0.7)

plt.subplots_adjust(hspace=.0)
plt.show()
